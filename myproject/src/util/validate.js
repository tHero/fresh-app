/**
 * Created by jiachenpan on 16/11/18.
 */

/* 合法uri */
export function validateURL (textval) {
  const urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/
  return urlregex.test(textval)
}

/* 小写字母 */
export function validateLowerCase (str) {
  const reg = /^[a-z]+$/
  return reg.test(str)
}

/* 大写字母 */
export function validateUpperCase (str) {
  const reg = /^[A-Z]+$/
  return reg.test(str)
}

/* 大小写字母 */
export function validatAlphabets (str) {
  const reg = /^[A-Za-z]+$/
  return reg.test(str)
}
export function isNumberic (rule, value, callback) {
  const reg = /^[0-9]+$/
  return reg.test(value)
}
export function mobile (value) {
  const reg = /^1[35][0-9]\d{8}$/
  return reg.test(value)
}
export function email (value) {
  const reg = /^[-_A-Za-z0-9]+@([_A-Za-z0-9]+\.)+[A-Za-z0-9]{2,3}$/
  return reg.test(value)
}
// eslint-disable-next-line camelcase
export function numberOr_OrLetterFirst (value) {
  const reg = /^[a-zA-Z][a-zA-Z0-9_]*$/
  return reg.test(value)
}
export function password (value) {
  const reg = /^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)[a-zA-Z\d!@#$%^&*]+$/
  return reg.test(value)
}
export var isEmptyStr = (rule, value, callback) => {
  if (value === undefined || value === '') {
    callback(new Error('该项为必填项'))
  }
}
export var isNumberics = (rule, value, callback) => {
  isEmptyStr(rule, value, callback)
  if (isNumberic(rule, value, callback)) {
    callback()
  } else {
    callback(new Error('请输入正整数 例如：1,2,25'))
  }
}
export var isMobile = (rule, value, callback) => {
  isEmptyStr(rule, value, callback)
  if (mobile(value)) {
    callback()
  } else {
    callback(new Error('请输入正确的手机号码'))
  }
}
export var isEmail = (rule, value, callback) => {
  isEmptyStr(rule, value, callback)
  if (email(value)) {
    callback()
  } else {
    callback(new Error('请输入正确的邮件格式 例如：abcdew@163.com'))
  }
}
// eslint-disable-next-line camelcase
export var isNumberOr_OrLetterFirst = (rule, value, callback) => {
  isEmptyStr(rule, value, callback)
  if (numberOr_OrLetterFirst(value)) {
    callback()
  } else {
    callback(new Error('只能输入英文字母、数字、下划线_，且以字母开头 例如：aads_73736jehwury_'))
  }
}
export var isPassword = (rule, value, callback) => {
  isEmptyStr(rule, value, callback)
  if (password(value)) {
    callback()
  } else {
    callback(new Error('必须是一下组合：字母+数字，字母+特殊字符，数字+特殊字符'))
  }
}
/**
 * validate email
 * @param email
 * @returns {boolean}
 */
export function validateEmail (email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(email)
}
