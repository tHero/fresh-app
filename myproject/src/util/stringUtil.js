export function coverArry2Str (tarArr, splitStr) {
  var result = ''
  if (tarArr === undefined || tarArr.length <= 0) {
    return result
  } else {
    for (var i = 0; i < tarArr.length; i++) {
      result += tarArr[i] + splitStr
    }
  }
  return result
}
