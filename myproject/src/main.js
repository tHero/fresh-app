// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vant from 'vant'
import 'vant/lib/index.css'
import vueg from 'vueg'
import '../static/css/transition-min.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import { far } from '@fortawesome/free-regular-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'

library.add(fas, far, fab)
Vue.config.productionTip = false
const options = {
  duration: '0.3', // 转场动画时长，默认为0.3，单位秒
  firstEntryDisable: true, // 值为true时禁用首次进入应用时的渐现动画，默认为false
  firstEntryDuration: '.6', // 首次进入应用时的渐现动画时长，默认为.6
  forwardAnim: 'fadeInRight', // 前进动画，默认为fadeInRight
  backAnim: 'fadeInLeft', // 后退动画，默认为fedeInLeft
  sameDepthDisable: false, // url深度相同时禁用动画，默认为false
  tabs: [{
    name: 'home'
  }, {
    name: 'my'
  }], // 默认为[]，'name'对应路由的name,以实现类似app中点击tab页面水平转场效果，如tabs[1]到tabs[0]，会使用backAnim动画，tabs[1]到tabs[2]，会使用forwardAnim动画
  tabsDisable: false, // 值为true时，tabs间的转场没有动画，默认为false
  shadow: true, // 值为false，转场时没有阴影的层次效果
  disable: false // 禁用转场动画，默认为false，嵌套路由默认为true
}
Vue.use(vueg, router, options)
Vue.use(Vant, vueg)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers)
Vue.component('font-awesome-layers-text', FontAwesomeLayersText)
Vue.component('font-awesome-icon', FontAwesomeIcon)

/* eslint-disable no-new */
if (typeof (window) === 'undefined') {
  document.addEventListener('deviceready', function () {
    new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
    document.addEventListener('jpush.receiveRegistrationId', function (event) {
    }, false)
    initiateUI()
  }, false)
} else {
  new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
  })
}
function initiateUI () {
  try {
    window.JPush.init()
    window.JPush.setDebugMode(true)
    getRegistrationID()
    // eslint-disable-next-line no-undef
    if (device.platform !== 'Android') {
      window.JPush.setApplicationIconBadgeNumber(0)
    }
  } catch (exception) {
    console.log(exception)
  }
}
function getRegistrationID () {
  window.JPush.getRegistrationID(onGetRegistrationID)
}
function onGetRegistrationID (data) {
  try {
    alert('JPushPlugin:registrationID is ' + data)
    if (data.length === 0) {
      window.setTimeout(getRegistrationID, 1000)
    }
  } catch (exception) {
    alert('ongetReg66' + exception)
  }
}
