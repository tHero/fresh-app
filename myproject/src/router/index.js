import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Camera from '@/components/Camera'
import PhotoLibrary from '@/components/PhotoLibrary'
import Gprs from '@/components/Gprs'
import Wechat from '@/components/Wechat'
import Contacts from '@/components/Contacts.vue'
import Icon from '@/components/Icon.vue'
import Database from '@/components/Database.vue'
import Main from '@/components/Main.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    }, {
      path: '/hello',
      name: 'HelloWorld',
      component: HelloWorld
    }, {
      path: '/camera',
      name: 'Camera',
      component: Camera
    },
    {
      path: '/gprs',
      name: 'Gprs',
      component: Gprs
    },
    {
      path: '/photoLibrary',
      name: 'photoLibrary',
      component: PhotoLibrary
    },
    {
      path: '/wechat',
      name: 'Wechat',
      component: Wechat
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: Contacts
    }, {
      path: '/icon',
      name: 'Icon',
      component: Icon
    }, {
      path: '/database',
      name: 'Database',
      component: Database
    }, {
      path: '/main',
      name: 'Main',
      component: Main
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
