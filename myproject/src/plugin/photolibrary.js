/* eslint-disable */
function photoLibrary() {

  var cameraOptions= {
    quality: 75,
    destinationType: Camera.DestinationType.DATA_URL,
    sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,    //相册类型
    allowEdit: true,
    encodingType: Camera.EncodingType.JPEG,
    targetWdith: 100,
    targetHeight: 100,
    popoverOptions: CameraPopoverOptions,
    saveToPhotoAlbum: false
  }
  navigator.camera.getPicture(onSuccess,onFail,cameraOptions);

  function onSuccess(imageData) {
    var image = document.getElementById('myImage');
    image.src = "data:image/jpeg;base64," + imageData;
  }

  function onFail(message) {
    alert('Failed because: ' + message);
  }
}
export {
  photoLibrary
}
