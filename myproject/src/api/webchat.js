import request from '../util/request'

export function rubbishCategory (query) {
  return request({
    url: 'api/rubbish/category',
    method: 'get',
    params: query
  })
};
// 通过code获取access_token
export function getToken (result) {
  return request({
    url: 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=wx6dec3d23db467fd2&secret=162ae5393062eb52c476d787782fccb9&code=' + result.code + '&grant_type=authorization_code',
    method: 'get',
    params: result
  })
};
// 通过access_token调用接口
export function getInterface (result) {
  return request({
    url: 'https://api.weixin.qq.com/sns/auth?access_token=' + result.data.access_token + '&openid=' + result.data.openid,
    method: 'get',
    params: result
  })
};
// 获取用户个人信息（UnionID机制）
export function getUserInfo (result) {
  return request({
    url: 'https://api.weixin.qq.com/sns/userinfo?access_token=' + result.data.access_token + '&openid=' + result.data.openid,
    method: 'get',
    params: result
  })
};
// 微信分享文字
export function shareText (text) {
  // eslint-disable-next-line no-undef
  Wechat.share({
    text: text,
    // eslint-disable-next-line no-undef
    scene: Wechat.Scene.SESSION // 分享的场景 TIMELINE朋友圈 SESSION会话 FAVORITE 收藏 默认朋友圈
  }, function () {
    alert('Success')
  }, function (reason) {
    alert('Failed: ' + reason)
  })
};

// 微信分享链接
export function shareLink (text) {
  // eslint-disable-next-line no-undef
  Wechat.share({
    message: {
      title: '这是分享的标题',
      description: '这是分享的描述',
      thumb: 'www/assets/imgs/logo.png',
      media: {
        // eslint-disable-next-line no-undef
        type: Wechat.Type.WEBPAGE, // webpage网页 image图片 music音乐 video视频 mini小程序
        webpageUrl: 'https://www.jason-z.com'
      }
    },
    // eslint-disable-next-line no-undef
    scene: Wechat.Scene.SESSION // 分享的场景 TIMELINE朋友圈 SESSION会话 FAVORITE 收藏 默认朋友圈
  }, function () {
    alert('Success')
  }, function (reason) {
    alert('Failed: ' + reason)
  })
};

// 微信分享图片
export function shareImage (text) {
  // eslint-disable-next-line no-undef
  Wechat.share({
    message: {
      title: '这是分享的标题',
      description: '这是分享的描述',
      thumb: 'www/assets/imgs/logo.png',
      media: {
        // eslint-disable-next-line no-undef
        type: Wechat.Type.IMAGE, // webpage网页 image图片 music音乐 video视频 mini小程序
        image: 'https://www.jason-z.com/storage/test_image.jpg'
      }
    },
    // eslint-disable-next-line no-undef
    scene: Wechat.Scene.SESSION // 分享的场景 TIMELINE朋友圈 SESSION会话 FAVORITE 收藏 默认朋友圈
  }, function () {
    alert('Success')
  }, function (reason) {
    alert('Failed: ' + reason)
  })
};

// 微信分享音乐
export function shareMusic (text) {
  // eslint-disable-next-line no-undef
  Wechat.share({
    message: {
      title: '这是分享的标题',
      description: '这是分享的描述',
      thumb: 'www/assets/imgs/logo.png',
      media: {
        // eslint-disable-next-line no-undef
        type: Wechat.Type.MUSIC,
        musicUrl: 'https://www.jason-z.com',
        musicDataUrl: 'https://www.jason-z.com/storage/test_audio.mp3'
      }
    },
    // eslint-disable-next-line no-undef
    scene: Wechat.Scene.SESSION // 分享的场景 TIMELINE朋友圈 SESSION会话 FAVORITE 收藏 默认朋友圈
  }, function () {
    alert('Success')
  }, function (reason) {
    alert('Failed: ' + reason)
  })
};

// 微信分享视频
export function shareVideo (text) {
  // eslint-disable-next-line no-undef
  Wechat.share({
    message: {
      title: '这是分享的标题',
      description: '这是分享的描述',
      thumb: 'www/assets/imgs/logo.png',
      media: {
        // eslint-disable-next-line no-undef
        type: Wechat.Type.VIDEO,
        videoUrl: 'https://www.jason-z.com/storage/test_video.mp4'
      }
    },
    // eslint-disable-next-line no-undef
    scene: Wechat.Scene.SESSION // 分享的场景 TIMELINE朋友圈 SESSION会话 FAVORITE 收藏 默认朋友圈
  }, function () {
    alert('Success')
  }, function (reason) {
    alert('Failed: ' + reason)
  })
};

// 微信分享小程序
export function shareMini (text) {
  // eslint-disable-next-line no-undef
  Wechat.share({
    message: {
      title: '这是分享的标题',
      description: '这是分享的描述',
      thumb: 'www/assets/imgs/logo.png',
      media: {
        // eslint-disable-next-line no-undef
        type: Wechat.Type.MINI,
        webpageUrl: 'https://www.jason-z.com', // 兼容低版本的网页链接
        userName: 'gh_745127d80c0f', // 小程序原始id
        path: 'pages/logs/logs', // 小程序的页面路径
        hdImageData: 'https://www.jason-z.com/storage/test_image.jpg', // 程序新版本的预览图二进制数据 不超过128kb 支持 地址 base64 temp
        withShareTicket: true, // 是否使用带shareTicket的分享
        // eslint-disable-next-line no-undef
        miniprogramType: Wechat.Mini.PREVIEW // 小程序类型：RELEASE发布版 TEST测试版 PREVIEW体验版
      }
    },
    // eslint-disable-next-line no-undef
    scene: Wechat.Scene.SESSION // 分享的场景 TIMELINE朋友圈 SESSION会话 FAVORITE 收藏 默认朋友圈
  }, function () {
    alert('Success')
  }, function (reason) {
    alert('Failed: ' + reason)
  })
};
