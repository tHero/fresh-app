### ios项目需要在info.plist文件添加下面的代码
```html
<key>NSCameraUsageDescription</key>
<string>useCam</string>
<key>NSContactsUsageDescription</key>
<string>useContact</string>
<key>NSMicrophoneUsageDescription</key>
<string>useMicrophone</string>
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
<string>My app requires constant access to your location, even when the screen is off.</string>
<key>NSLocationWhenInUseUsageDescription</key>
<string>My app requires constant access to your location, even when the screen is off.</string>
```
