1、在极光推送官网注册账号、应用，注意注册的包名要和app的包名一致

2、添加插件
```javascript
    cordova plugin add jpush-phonegap-plugin --variable APP_KEY=you key 
```

3、在app.js的$ionicPlatform.ready方法中初始化极光服务

```js
if (typeof (window) === 'undefined') {
  document.addEventListener('deviceready', function () {
    new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
    document.addEventListener('jpush.receiveRegistrationId', function (event) {
    }, false)
    initiateUI()
  }, false)
} else {
  new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
  })
}
function initiateUI () {
  try {
    window.JPush.init()
    window.JPush.setDebugMode(true)
    getRegistrationID()
    // eslint-disable-next-line no-undef
    if (device.platform !== 'Android') {
      window.JPush.setApplicationIconBadgeNumber(0)
    }
  } catch (exception) {
    console.log(exception)
  }
}
function getRegistrationID () {
  window.JPush.getRegistrationID(onGetRegistrationID)
}
function onGetRegistrationID (data) {
  try {
    alert('JPushPlugin:registrationID is ' + data)
    if (data.length === 0) {
      window.setTimeout(getRegistrationID, 1000)
    }
  } catch (exception) {
    alert('ongetReg66' + exception)
  }
}

```

4、用户登录后，设置Tag，Tag关联用户的角色：

4、服务端推送消息：

