### cordova读取联系人

#### 执行下面的命令

```
cordova plugin add cordova-plugin-contacts
```

读取代码

```js?linenums
    getContacts () {
      let _this = this
      let contactFileds = ['displayName', 'name', 'phoneNumbers', 'emails', 'address']
      let options = { filter: '', multiple: true }
      navigator.contacts.find(contactFileds, _this.success, _this.onError, options)
    },
    success (contacts) {
      var _this = this
      for (let i = 0; i < contacts.length; i++) {
        let itemContact = contacts[i]
        let item = {
          id: itemContact.id,
          displayName: itemContact.displayName,
          phoneNumbers: itemContact.phoneNumbers[0].value
        }
        _this.contactList.push(item)
      }
    },
    onError (err) {
      let _this = this
      _this.app = JSON.stringify(err)
      _this.toast.clear()
    }
```

详细可参照项目中的`Contacts.vue`