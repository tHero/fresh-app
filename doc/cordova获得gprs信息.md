> 添加插件信息

```
cordova plugin add org.apache.cordova.geolocation
cordova plugin add cordova-plugin-geolocation
```
> 在`android`的文件中添加下面的权限路径为`platforms/android/app/src/main/AndroidManifest.xml`

```html
    <uses-permission android:name="android.permission.READ_CONTACTS" />
    <uses-permission android:name="android.permission.WRITE_CONTACTS" />
    <uses-permission android:name="android.permission.GET_ACCOUNTS" />
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
    <uses-permission android:name="android.permission.VIBRATE" />
    <uses-permission android:name="android.permission.RECEIVE_BOOT_COMPLETED" />
```
> 在`config.xml`文件中添加路径为`platforms/android/app/src/main/res/xml/config.xml`

```html
    <feature name="Geolocation">
        <param name="android-package" value="org.apache.cordova.geolocation.GeoBroker" />
    </feature>
    <feature name="Geolocation">
        <param name="android-package" value="org.apache.cordova.geolocation.Geolocation" />
    </feature>
```

> 在`ios`的问题中添加路径为`platforms/ios/HelloWord/HelloWord-Info.plist`

```html
<key>NSLocationAlwaysAndWhenInUseUsageDescription</key>
	<string>My app requires constant access to your location, even when the screen is off.</string>
	<key>NSLocationWhenInUseUsageDescription</key>
	<string>My app requires constant access to your location, even when the screen is off.</string>

```

根据上面的配置方式不会出现`time out`的问题
