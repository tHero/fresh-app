### apk签名
在项目根目录下运行命令
```js
cordova build --release android
```
会在testApp\platforms\android\build\outputs\apk目录下生成一个android-release-unsigned.apk

### 生成签名

运行命令
```js
keytool -genkeypair -alias name.keystore -keyalg RSA -validity 4000 -keystore name.keystore 
```
执行以上命令后，会要求填写密码口令，单位信息等等，这里需要记住录入的密码，因为最后编译apk的时候还需要用到，在所有的选项都录入完后，按回车，会在项目的根目录下生成一个name.keystore的签名文件，里面就包含刚刚录入的一些信息。
会在根目录下生成一个name.keystore,这是apk独有的签名证书
### 打包签名
将testApp\platforms\android\build\outputs\apk目录下生成一个androidgned.apk
(我为了与name.keystore对应)，并将它和根目录下的name.keystore放在同一目录下，运行
```js
jarsigner -verbose -keystore name.keystore -signedjar name.apk name_unsigned.apk name.keystore
```
输入之前签名的录入的密码，经过编译，会生成最后的签名版本 name.apk.
